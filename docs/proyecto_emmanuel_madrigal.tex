\documentclass{IEEEconf}

\input{macros}

\author{Emmanuel Madrigal}

\title{Evaluation of segmentation algorithms on spores and water}

\begin{document}

\maketitle

\begin{abstract}
  This paper performs an evaluation of video segmentation algorithms for the segmenting of water and spore videos. The test videos correspond to high-speed captures of both of these phenomenons separately. The different methods have different success rates at segmenting the different objects, only the Swin Transformer is able to segment the spore ``cloud'' but it was unable to detect the water in any of its stages. While DetectoRS and SOLOv2 are able to detect the water splash but are only partially successful at detecting some spore lumps. SFSegFlowNet is unable to detect any object in particular but has some large activations when the splash is occurring. The DetectoRS is the only network able to detect a water droplet but only for a single frame.
\end{abstract}

\section{Introduction}

Semantic segmentation has been a fundamental challenge in computer vision.  The goal is to classify each pixel in an image into one of a given set of categories~\cite{Xu2018}.

It therefore combines elements from the classical computer vision tasks of object detection, where the goal is to classify individual objects and localize each using a bounding box, and semantic segmentation, where the goal is to classify each pixel into a fixed set of categories without differentiating object instances~\cite{He2017}.

It is unnecessary to reprocess every single pixel of a frame by those deep semantic segmentation models in a video sequence. When comparing the difference between two consecutive frames, in most cases, a large portion of them is similar~\cite{Xu2018}. Therefore, performing complex semantic segmentation on the entire video frame can potentially be a waste of time on one hand, and on the other, the redundant information between frames can be used to increase the overall accuracy.

\section{State of the art}

\subsection{SFSegFlowNet}

The SFSegFlowNet~\cite{Li2020} proposes a ``flow aligment modules'' which learns semantic flow between feature maps of adjacent levels, and broadcasts high level features to high resolution features features efficiently. This allows it merge features from multiple pyramidal levels enriching the segmentation masks, it also allows for an alternative combination of feature maps from different frames to increase performance. This method reaches an mIoU of 80.4 in the CityScapes challenge~\cite{cityscapes2016}.

\subsection{SOLOv2}

The SOLOv2~\cite{Wang2020} method implements a direct instance segmentation. It does this without having a previous bounding box detection, but rather directly determining the segmentation mask and the corresponding instance that it corresponds to. This method reaches an AP of 41.7\% the COCO test-dev~\cite{lin2014microsoft} challenge.

\subsection{DetectorRS}

The DetectoRS~\cite{Qiao2020} which uses several feedback connections from the top-down FPN layers to the backbone, and it also uses multiple prediction outputs that can be switched to reach a higher consensus on the final object. This method reaches an AP of 51.3\% in the COCO test-dev challenge.

\subsection{Swin Transformer}

The Swin Transformer method~\cite{Liu2021} proposes a vision Transformer that also serves as a general-purpose backbone for computer vision (The authors also show this method in \textit{classification} and \textit{detection} tasks which aren't relevant to the current paper). The method uses a hierarchical Transformer whose representation is computed with shifted windows. This method reaches an AP of 58.7\% in the COCO test-dev challenge.

\section{Methodology}

The evaluation will be performed on different video and image segmentation methods, these methods are selected on the basis of performance and code availability. 

Each of these will be evaluated on each of the following image sequences:
\begin{itemize}
  \item Static water setup.
  \item Water droplet falling.
  \item Water splash.
  \item Spore ``cloud'' moving.
\end{itemize}

These sequences are extracted from the high-speed video captures provided by~\cite{GarciaBrenes2021}.

Since these are trained to perform segmentation on specific categories the detection will be evaluated qualitatively since a quantitative evaluation would require the ground-truth labels for these objects and a previous retraining so that the output categories match the video objects.

\section{Results}

\subsection{SFSegNet}

In Figure~\ref{fig:water_setup}, the water setup with no other object is shown. This includes a ruler, a base, and the background. The segmentation network can partially differentiate between the background/ruler and the base. Since these networks weren't trained to detect any of these objects the detection correspond to some other networks, the yellow detection in the background and ruler corresponds to \textit{Traffic Sign, Direction Front}, and the base is partially covered by a blue detection which matches a \textit{Sky} object.

\begin{figure*}[htbp]
  \centering
  \includegraphics[width=0.5\textwidth]{images/sfsegnet/base_water_setup/overlap_frame_00001.png}
  \caption{Sample of the water tracking setup segmentation mask using SFSegNet network}
  \label{fig:water_setup}
\end{figure*}

Figure~\ref{fig:sfsegnet_water_droplet} shows the same setup as before, but now with a water droplet falling. The two labels from the setup are still being detected, however, the water droplet or its shadow still doesn't cause any changes in any of the detections.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_with_droplet/overlap_frame_00283.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_with_droplet/overlap_frame_00284.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_with_droplet/overlap_frame_00285.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_with_droplet/overlap_frame_00286.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_with_droplet/overlap_frame_00287.png}
  \end{subfigure}
  \caption{Sample of water droplet segmentation mask using SFSegNet network}
  \label{fig:sfsegnet_water_droplet}
\end{figure*}

Figure~\ref{fig:sfsegnet_water_splash} shows the same droplet after it has crashed and caused a splash. Here, the splash causes changes in the detection, while the background and base mostly remain with the same detection. The splash causes a change in the detection, having a dark blue \textit{Bus} detection and a smaller \textit{Pole} detection. The dark blue detection is not only limited to the water splash but also ``bleeds'' into the background and the base; while the grey detection is mostly limited to the water splash.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00289.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00290.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00291.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00292.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00293.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00294.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00295.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00296.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00297.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/water_splash/overlap_frame_00298.png}
  \end{subfigure}
  \caption{Sample of water splash segmentation mask using SFSegNet network}
  \label{fig:sfsegnet_water_splash}
\end{figure*}

Figure~\ref{fig:sfsegnet_spores_mask} shows the same network segmenting a sequence of spores moving. The background, in this case, remains a ``Fence'' detection, which doesn’t change when the spores are moving or when these are absent.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00775.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00780.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00785.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00790.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00795.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00800.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00805.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00810.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00815.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/sfsegnet/spores/overlap_frame_00825.png}
  \end{subfigure}
  \caption{Sample of spores segmentation mask using SFSegNet network}
  \label{fig:sfsegnet_spores_mask}
\end{figure*}

\subsection{SOLOv2}

The segmentation mask for the setup using the SOLOv2 network is shown in figure~\ref{fig:solov2_water_setup}. The base is separated from the background and the ruler, and the network is also able to segment some of the water droplets shown in the base.

\begin{figure*}[htbp]
  \centering
  \includegraphics[width=0.5\textwidth]{images/solov2/base_water_setup/frame_00001.png}
  \caption{Sample of the water tracking setup segmentation mask using SOLOv2 network}
  \label{fig:solov2_water_setup}
\end{figure*}

Figure~\ref{fig:solov2_water_droplet} shows the segmentation mask for the water droplets. In none of the frames, the water droplet nor its shadow is correctly segmented, however, just as in figure~\ref{fig:solov2_water_setup} some of the droplets leftover in the base are segmented. There is also a clear segmentation of the base from the rest of the setup, and in one of the other frames the ruler is correctly segmented, and in another the ruler and part of the background are segmented.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_with_droplet/frame_00283.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_with_droplet/frame_00284.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_with_droplet/frame_00285.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_with_droplet/frame_00286.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_with_droplet/frame_00287.png}
  \end{subfigure}
  \caption{Sample of water droplet segmentation mask using SOLOv2 network}
  \label{fig:solov2_water_droplet}
\end{figure*}

The segmentation mask for the water splash is shown in figure~\ref{fig:solov2_water_splash}, in most of these frames, the water splash is segmented, sometimes by multiple masks.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00289.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00290.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00291.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00292.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00293.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00294.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00295.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00296.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00297.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/water_splash/frame_00298.png}
  \end{subfigure}
  \caption{Sample of water splash segmentation mask using SOLOv2 network}
  \label{fig:solov2_water_splash}
\end{figure*}

In the case of the spores, figure~\ref{fig:solov2_spores_mask} shows the segmentation masks. In most of these, the spores aren't segmented, however in two of these frames, some of the larger spores are segmented from the rest of the background.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00775.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00780.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00785.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00790.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00795.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00800.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00805.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00810.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00815.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/solov2/spores/frame_00825.png}
  \end{subfigure}
  \caption{Sample of spores segmentation mask using SOLOv2 network}
  \label{fig:solov2_spores_mask}
\end{figure*}

\subsection{DetectorRS}

Figure~\ref{fig:detectors_water_setup} shows the segmented setup, in this case, the background, ruler, and base are correctly segmented as three different objects.

\begin{figure*}[htbp]
  \centering
  \includegraphics[width=0.5\textwidth]{images/detectors/frame_00001.png}
  \caption{Sample of the water tracking setup segmentation mask using DetectoRS network}
  \label{fig:detectors_water_setup}
\end{figure*}

The segmentation mask for the water falling is shown in figure~\ref{fig:detectors_water_droplet}. Most of these masks have a similar segmentation from the setup where the droplet is ignored, however the last image correctly segments the water droplet as an individual object with the ``apple'' label.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00283.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00284.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00285.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00286.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00287.png}
  \end{subfigure}
  \caption{Sample of water droplet segmentation mask using DetectoRS network}
  \label{fig:detectors_water_droplet}
\end{figure*}

Figure~\ref{fig:detectors_water_splash} shows the segmentation mask for a water splash. Once this is happening the network is no longer able to segment the background as an individual image. The network is also able to detect the location of the splash in some of the frames.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00289.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00290.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00291.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00292.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00293.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00294.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00295.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00296.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00297.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00298.png}
  \end{subfigure}
  \caption{Sample of water splash segmentation mask using DetectoRS network}
  \label{fig:detectors_water_splash}
\end{figure*}

The spores segmentation mask is shown in figure~\ref{fig:detectors_spores_mask}. The general shape of the spores is not correctly segmented, however there is a larger amount of the spores being detected as ``birds`` dual objects, and this detection is correctly performed in every single frame.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00775.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00780.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00785.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00790.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00795.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00800.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00805.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00810.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00815.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/detectors/frame_00825.png}
  \end{subfigure}
  \caption{Sample of spores segmentation mask using DetectoRS network}
  \label{fig:detectors_spores_mask}
\end{figure*}

\subsection{Swin Transformer}

Figure~\ref{fig:swintransformer_water_setup} shows the segmentation of the setup, although there is a clear delineation between ruler/background and the base, the base isn't segmented as a single object. The ruler is also partially segmented as a separate object, while the background is also segmented is a single object.

\begin{figure*}[htbp]
  \centering
  \includegraphics[width=0.5\textwidth]{images/swintransformer/frame_00001.png}
  \caption{Sample of the water tracking setup segmentation mask using Swin Transformer network}
  \label{fig:swintransformer_water_setup}
\end{figure*}

The segmentation mask for the water droplet is shown in figure~\ref{fig:swintransformer_water_droplet}, these masks are almost the same as the previous image since the water droplet isn't segmented in any of the frames.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00283.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00284.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00285.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00286.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00287.png}
  \end{subfigure}
  \caption{Sample of water droplet segmentation mask using Swin Transformer network}
  \label{fig:swintransformer_water_droplet}
\end{figure*}

Figure~\ref{fig:swintransformer_water_splash} shows the segmentation mask for the water splash. The splash isn't correctly detected nor does it cause a major change in the detection of the other segments.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00289.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00290.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00291.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00292.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00293.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00294.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00295.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00296.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00297.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00298.png}
  \end{subfigure}
  \caption{Sample of water splash segmentation mask using Swin Transformer network}
  \label{fig:swintransformer_water_splash}
\end{figure*}

The segmentation mask for the spores movement is shown in figure~\ref{fig:swintransformer_spores_mask}. Although the spore mask isn't perfectly around the spore ``cloud'' however there is a large amount of detections in the general region where the spores are moving.

\begin{figure*}[htbp]
  \centering
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00775.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00780.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00785.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00790.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00795.png}
  \end{subfigure}
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00800.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00805.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00810.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00815.png}
  \end{subfigure}%
  \begin{subfigure}{0.2\textwidth}
    \includegraphics[width=\textwidth]{images/swintransformer/frame_00825.png}
  \end{subfigure}
  \caption{Sample of spores segmentation mask using Swin Transformer network}
  \label{fig:swintransformer_spores_mask}
\end{figure*}


\section{Conclusions}

This paper evaluated 4 different methods using their pretrained models in order to evaluate their current capacity for image segmentation of spores and water droplets.

Only the DetectoRS managed to detect the water droplet as an object, however this was only during a single frame.

SOLOv2 and DetectoRS managed to correctly distinguish the the region where the water splash was occurring, while the Swin Transformer didn't have any activation when this ocurred.
Although SFSegNet didn't have a clear segmentation mask for the water splash, however when it was present the methods presented large changes in its segmentation mask.

All methods except for SFSegNet where able to distinguish between the ruler, base and background ``areas'', however these sometimes stopped or increased this segmentation based on the amount of water droplets or splash present.

Only Swin Transformer was able to perform some amount of segmentation on the spores ``cloud'', while DetectoRS and in couple of cases SOLOv2 where able to detect some of the larger lumps of spores.


\printbibliography

\newpage
\end{document}

