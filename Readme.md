# Video Segmentation on Water and Spore videos

This Repository documents how to run the SFSegNets and the CCNet for video segmentation.

This repo includes the testing projects as submodules. Make sure to run before following the next steps:
```
git submodule update --init --recursive
```

**Note**: All demos require a CUDA device to run.

## SFSegNets

The best way to all the needed dependencies is to use a virtual environment to install all dependencies. The following code uses `conda` for this purpose:

```bash
conda create --name SFSegNets -c conda-forge opencv  pytorch-gpu   nvidia-apex torchvision pillow scipy scikit-image tensorboardx
```

And make sure to activate it for all further tests in this section:

```bash
conda activate SFSegNets
```

The SFSegNets submodule includes general instruction to get the repository running, however, it might need downloading of some large datasets and weights, the following instructions

Download the following files:

sf-resnet18-Mapillary:[link](https://drive.google.com/file/d/1Hq7HhszrAicAr2PnbNN880ijAYcxJJ0I/view?usp=sharing)

resnet18-deep-stem-pytorch:[link](https://drive.google.com/file/d/16mcWZSWbV3hkFWJ2cP_eJRQ6Nr1BncCp/view?usp=sharing)

And save it to `SFSegNets/pretrained_models`.

Copy all desired images for inference to a `images` folder.

The repository also needs a few changes before being ran succesfuly:
```bash
# Copy the needed configuration files (this normally requires downloading the mapillary dataset)
cd SFSegNets
mkdir -p data/mapillary
cp ../utils/config* data/mappilary/
# Apply the patch to fix library API errors
git apply ../utils/sfsegnets.patch
```



And then run the demo as follows:
```bash
python demo_floder.py --snapshot pretrained_models/sfnet_r18_map.pth --demo_folder images --save_dir output_dir
```

**Setup**

![](docs/images/sfsegnet/base_water_setup/overlap_frame_00001.png)

**Water Droplet**

![](docs/images/sfsegnet/water_with_droplet/overlap_frame_00283.png)
![](docs/images/sfsegnet/water_with_droplet/overlap_frame_00284.png)
![](docs/images/sfsegnet/water_with_droplet/overlap_frame_00285.png)
![](docs/images/sfsegnet/water_with_droplet/overlap_frame_00286.png)
![](docs/images/sfsegnet/water_with_droplet/overlap_frame_00287.png)
![](docs/images/sfsegnet/water_with_droplet/overlap_frame_00288.png)

**Water Splash**

![](docs/images/sfsegnet/water_splash/overlap_frame_00289.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00290.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00291.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00292.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00293.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00294.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00295.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00296.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00297.png)
![](docs/images/sfsegnet/water_splash/overlap_frame_00298.png)

**Spores**

![](docs/images/sfsegnet/spores/overlap_frame_00775.png)
![](docs/images/sfsegnet/spores/overlap_frame_00780.png)
![](docs/images/sfsegnet/spores/overlap_frame_00785.png)
![](docs/images/sfsegnet/spores/overlap_frame_00790.png)
![](docs/images/sfsegnet/spores/overlap_frame_00795.png)
![](docs/images/sfsegnet/spores/overlap_frame_00800.png)
![](docs/images/sfsegnet/spores/overlap_frame_00805.png)
![](docs/images/sfsegnet/spores/overlap_frame_00810.png)
![](docs/images/sfsegnet/spores/overlap_frame_00815.png)
![](docs/images/sfsegnet/spores/overlap_frame_00820.png)
![](docs/images/sfsegnet/spores/overlap_frame_00825.png)





## AdelaiDet

```bash
conda create --name AdelaiDet -c conda-forge pytorch-gpu torchvision opencv
conda activate AdelaiDet
```

### SOLOv2

First install Detectron:
```bash
python -m pip install 'git+https://github.com/facebookresearch/detectron2.git'
```

Then install the AdelaiDet:

```bash
cd AdelaiDet
python setup.py build develop
wget https://cloudstor.aarnet.edu.au/plus/s/chF3VKQT4RDoEqC/download -O SOLOv2_R50_3x.pth
python demo/demo.py \
    --config-file configs/SOLOv2/R50_3x.yaml \
    --input input1.jpg input2.jpg \
    --output output_dir \
    --opts MODEL.WEIGHTS SOLOv2_R50_3x.pth
```

**Note**: This network also includes other networks which are not tested in these project.


**Setup**

![](docs/images/solov2/base_water_setup/frame_00001.png)

**Water Droplet**

![](docs/images/solov2/water_with_droplet/frame_00283.png)
![](docs/images/solov2/water_with_droplet/frame_00284.png)
![](docs/images/solov2/water_with_droplet/frame_00285.png)
![](docs/images/solov2/water_with_droplet/frame_00286.png)
![](docs/images/solov2/water_with_droplet/frame_00287.png)
![](docs/images/solov2/water_with_droplet/frame_00288.png)

**Water Splash**

![](docs/images/solov2/water_splash/frame_00289.png)
![](docs/images/solov2/water_splash/frame_00290.png)
![](docs/images/solov2/water_splash/frame_00291.png)
![](docs/images/solov2/water_splash/frame_00292.png)
![](docs/images/solov2/water_splash/frame_00293.png)
![](docs/images/solov2/water_splash/frame_00294.png)
![](docs/images/solov2/water_splash/frame_00295.png)
![](docs/images/solov2/water_splash/frame_00296.png)
![](docs/images/solov2/water_splash/frame_00297.png)
![](docs/images/solov2/water_splash/frame_00298.png)

**Water Splash**

![](docs/images/solov2/spores/frame_00775.png)
![](docs/images/solov2/spores/frame_00780.png)
![](docs/images/solov2/spores/frame_00785.png)
![](docs/images/solov2/spores/frame_00790.png)
![](docs/images/solov2/spores/frame_00795.png)
![](docs/images/solov2/spores/frame_00800.png)
![](docs/images/solov2/spores/frame_00805.png)
![](docs/images/solov2/spores/frame_00810.png)
![](docs/images/solov2/spores/frame_00815.png)
![](docs/images/solov2/spores/frame_00820.png)
![](docs/images/solov2/spores/frame_00825.png)

## DetectoRS

```bash
conda create -n DetectoRS
conda activate DetectoRS
conda install pytorch=1.6.0 torchvision cudatoolkit=10.1 -c pytorch


pip install mmcv-full==1.3.3
python -m pip install 'git+https://github.com/open-mmlab/mmdetection.git'

pip install terminaltables timm
```

```bash
cd mmdetection
wget http://download.openmmlab.com/mmdetection/v2.0/detectors/detectors_htc_r50_1x_coco/detectors_htc_r50_1x_coco-329b1453.pth

python demo/image_demo.py \
    $IMAGE_FILE \
    configs/detectors/detectors_htc_r50_1x_coco.py \
    detectors_htc_r50_1x_coco-329b1453.pth \
    --device cuda:0
```

**Setup**

![](docs/images/detectors/frame_00001.png)

**Water Droplet**
![](docs/images/detectors/frame_00283.png)
![](docs/images/detectors/frame_00284.png)
![](docs/images/detectors/frame_00285.png)
![](docs/images/detectors/frame_00286.png)
![](docs/images/detectors/frame_00287.png)
![](docs/images/detectors/frame_00288.png)

**Water Splash**

![](docs/images/detectors/frame_00289.png)
![](docs/images/detectors/frame_00290.png)
![](docs/images/detectors/frame_00291.png)
![](docs/images/detectors/frame_00292.png)
![](docs/images/detectors/frame_00293.png)
![](docs/images/detectors/frame_00294.png)
![](docs/images/detectors/frame_00295.png)
![](docs/images/detectors/frame_00296.png)
![](docs/images/detectors/frame_00297.png)
![](docs/images/detectors/frame_00298.png)

**Spores**

![](docs/images/detectors/frame_00775.png)
![](docs/images/detectors/frame_00780.png)
![](docs/images/detectors/frame_00785.png)
![](docs/images/detectors/frame_00790.png)
![](docs/images/detectors/frame_00795.png)
![](docs/images/detectors/frame_00800.png)
![](docs/images/detectors/frame_00805.png)
![](docs/images/detectors/frame_00810.png)
![](docs/images/detectors/frame_00815.png)
![](docs/images/detectors/frame_00825.png)

## Swin Transformer

```bash
conda create -n SwinTransformer
conda activate SwinTransformer
conda install pytorch=1.6.0 torchvision cudatoolkit=10.1 -c pytorch

pip install mmcv-full==1.3.0
python -m pip install 'git+https://github.com/SwinTransformer/Swin-Transformer-Semantic-Segmentation.git' # This is a fork of mmsegmentation
```

```bash
cd Swin-Transformer-Semantic-Segmentation

wget https://github.com/SwinTransformer/storage/releases/download/v1.0.1/upernet_swin_base_patch4_window7_512x512.pth

python demo/image_demo.py \
    $IMAGE_FILE \
    configs/swin/upernet_swin_base_patch4_window7_512x512_160k_ade20k.py \
    upernet_swin_base_patch4_window7_512x512.pth \
    --device cuda:0 --palette ade20k
```

**Setup**

![](docs/images/swintransformer/frame_00001.png)

**Water Droplet**

![](docs/images/swintransformer/frame_00283.png)
![](docs/images/swintransformer/frame_00284.png)
![](docs/images/swintransformer/frame_00285.png)
![](docs/images/swintransformer/frame_00286.png)
![](docs/images/swintransformer/frame_00287.png)
![](docs/images/swintransformer/frame_00289.png)

**Water Splash**

![](docs/images/swintransformer/frame_00290.png)
![](docs/images/swintransformer/frame_00291.png)
![](docs/images/swintransformer/frame_00292.png)
![](docs/images/swintransformer/frame_00293.png)
![](docs/images/swintransformer/frame_00294.png)
![](docs/images/swintransformer/frame_00295.png)
![](docs/images/swintransformer/frame_00296.png)
![](docs/images/swintransformer/frame_00297.png)
![](docs/images/swintransformer/frame_00298.png)

**Spores**

![](docs/images/swintransformer/frame_00775.png)
![](docs/images/swintransformer/frame_00780.png)
![](docs/images/swintransformer/frame_00785.png)
![](docs/images/swintransformer/frame_00790.png)
![](docs/images/swintransformer/frame_00795.png)
![](docs/images/swintransformer/frame_00800.png)
![](docs/images/swintransformer/frame_00805.png)
![](docs/images/swintransformer/frame_00810.png)
![](docs/images/swintransformer/frame_00815.png)
![](docs/images/swintransformer/frame_00825.png)